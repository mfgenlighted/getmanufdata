﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using FixtureDatabase;

namespace GetManufData
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void configureToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string currentIP = string.Empty;

            currentIP = Properties.Settings.Default.ServerIP;
            if (MessageBox.Show("Current IP is " + currentIP + ". Do you want to change it?", "Server IP", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                GetIPAddress iform = new GetIPAddress();
                iform.ShowDialog();
                Properties.Settings.Default.ServerIP = iform.IPAddress;
                Properties.Settings.Default.Save();
                iform.Hide();
                iform.Dispose();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string productCode = string.Empty;
            string pcbaSN = string.Empty;
            string mac = string.Empty;

            if (this.tbHLASN.TextLength != 15)
            {
                MessageBox.Show("Invalid serial number. Must be 15 characters long.");
                return;
            }
            productCode = this.tbHLASN.Text.Substring(1, 3);            // get product code from serial number
            using (SerialNumberDatabase snDb = new SerialNumberDatabase())
            {
                try
                {
                    if (!snDb.Open(Properties.Settings.Default.ServerIP, "serialnumbersused", Properties.Settings.Default.user, Properties.Settings.Default.password))
                    {
                        MessageBox.Show("Product code is not in the database");
                        snDb.Close();
                        return;
                    }
                    if (!snDb.LookForHLASN(productCode, this.tbHLASN.Text, ref pcbaSN, ref mac))
                    {
                        MessageBox.Show("Serial number not in database");
                        snDb.Close();
                        return;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("problem with database: " + ex.Message);
                }
                MessageBox.Show("PCBA SN: " + pcbaSN + "  MAC: " + mac);
                snDb.Close();
            }
        }
    }
}
