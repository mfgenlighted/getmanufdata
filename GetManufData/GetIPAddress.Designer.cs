﻿namespace GetManufData
{
    partial class GetIPAddress
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tbIPAddress = new System.Windows.Forms.TextBox();
            this.bDone = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // tbIPAddress
            // 
            this.tbIPAddress.Location = new System.Drawing.Point(12, 21);
            this.tbIPAddress.Name = "tbIPAddress";
            this.tbIPAddress.Size = new System.Drawing.Size(151, 20);
            this.tbIPAddress.TabIndex = 0;
            // 
            // bDone
            // 
            this.bDone.Location = new System.Drawing.Point(123, 63);
            this.bDone.Name = "bDone";
            this.bDone.Size = new System.Drawing.Size(75, 23);
            this.bDone.TabIndex = 1;
            this.bDone.Text = "Done";
            this.bDone.UseVisualStyleBackColor = true;
            this.bDone.Click += new System.EventHandler(this.bDone_Click);
            // 
            // GetIPAddress
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(288, 115);
            this.Controls.Add(this.bDone);
            this.Controls.Add(this.tbIPAddress);
            this.Name = "GetIPAddress";
            this.Text = "GetIPAddress";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox tbIPAddress;
        private System.Windows.Forms.Button bDone;
    }
}