﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GetManufData
{
    public partial class GetIPAddress : Form
    {
        public string IPAddress = string.Empty;

        public GetIPAddress()
        {
            InitializeComponent();
        }

        private void bDone_Click(object sender, EventArgs e)
        {
            IPAddress = this.tbIPAddress.Text;
            DialogResult = DialogResult.OK;
            return;
        }
    }
}
